/**
 * 了解 弱类型 强类型
 * 
*/

void main(List<String> args) {
  // 弱类型  var object dynamic
  // 强类型 int string bool double datetime
  // 变量声明后默认都是null

  var a; // var a = ""; // 一旦赋值，就确定类型，不能随意改动
  a = "hello";

  Object b = "abc"; //动态任意类型

  // dynamic 动态任意类型
  // 编译阶段不检查类型
  dynamic c = "ssdgde";

 // var 简化定义变量
  var map = <String,dynamic>{};
  map["image"] = "image.png";
  map["title"] = "文本";
  map['desc'] = "描述";
  
}