void main(List<String> args) {
  // 定义
  int add(int x){
    return x + 1;
  }
  add(1);
  print(add(1));

  // 可选参数
  int addI(int x,[int? y,int? z]){
    if(y == null){
      y =1;
    }
    if(z == null){
      z = 1;
    }
    return x + y + z;
  }
  addI(1,2);
  print(addI(1,2));

  // 可选参数 默认值
  int add3(int x,[int y =1,int z = 2]){
    return x + y;
  }
  print(add3(1,2));

  // 命名参数，默认值
  int add4({int x = 1,int y = 1,int z = 1}){
      return x + y + z;
  }
  print(add4(x: 1,y: 2));

  // Funcation 返回函数对象
  Function makeAdd(int x){
    return (int y) => x + y;
  }

  var add5 = makeAdd(1);
  print(add5(5));

}