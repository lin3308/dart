void main(){
  int sum(int a,int b){
    var e = a +b;
    return e;
  }

  var result = sum(2, 3);
  print(result);  // 5
  /*
  bool isEven(int x){
    return x%2 ==0?true:false;
  }
   */
  bool isEven(int x) => x%2 == 0?true:false;
  print(isEven(6)); // true

  // 可选命名参数
  void message(String from,String content,{DateTime? time,String? device}){
    print('来自$from,内容:$content');
    if(time != null){
      print('时间:$time');
    }
    if(device != null){
      print('设备:$device');
    }
  }
  // 可选位置参数使用[]包裹可选参数列表
  // 在调用带有可选位置参数的函数时，应当按照参数的定义顺序
  // 依次赋值，可选位置参数列表中，前一个参数被赋值后，后一个参数才能被赋值

  message('json', 'hello', time: null); //来自json,内容:hello
  message('json', 'hello',time:DateTime.now()); //时间:2022-01-06 20:16:00.552990
  message('json', 'hello',time:DateTime.now(),device: 'pc'); //时间:2022-01-06 20:16:00.555985 设备:pc
  

  



}