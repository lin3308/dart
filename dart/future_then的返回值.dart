import 'dart:io';

void main(){
  
  new File(r"C:\Users\one\Desktop\1.txt").readAsString().then((String s){
    // 返回void 或者Future
    print(s);
    return 10; // 返回值 到另一个then
  }).then((int i){
    print(i); // 10
  });

  // then 可以得到Future 的结果并且返回一个新Future
  Future<String> getDate = new File(r"C:\Users\one\Desktop\1.txt").readAsString().then((String s){
    print(s); // hello world
    return "555";

  });
  getDate.then((String i){
    print(i); // 555
  });

  // 通过then 可以完成 有序任务的执行 一个任务执行完成后之后，下一个根据上个任务的结果，执行不同的操作
  
}