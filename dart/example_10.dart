class Point{
  num? x,y;

  Point({this.x, this.y});
  Point.origin(){
    this.x = 0;
    this.y = 0;
  }
  /*
  Point.fromJson(Map json){
    this.x = json['x'];
    this.y = json['y'];
  }
  */
  Point.fromJson(Map json):this.x = json['x'],this.y = json['y'];

  Point.alongXAxis(num x):this(x:x,y:9);
}

void main(){
  var p1 = Point(x:1);
  var p2 = Point(x:2,y:3);
  var p3 = Point();
  print('p1 x:${p1.x},p1 y:${p1.y}'); //p1 x:1,p1 y:null
  print('p2 x:${p2.x},p2 y:${p2.y}'); //p2 x:2,p2 y:3
  print('p3 x:${p3.x},p3 y:${p3.y}'); // p3 x:null,p3 y:null
  
  var p4 = Point.origin();
  print('p4 x:${p4.x},p4 y:${p4.y}'); //p4 x:0,p4 y:0

  var p5 = Point.fromJson({'x':3,'y':6});
  print('p5 x:${p5.x},p5 y:${p5.y}'); // p5 x:3,p5 y:6

  var p6 = Point.alongXAxis(6);
  print('p6 x:${p6.x},p6 y:${p6.y}'); //p6 x:6,p6 y:9

}