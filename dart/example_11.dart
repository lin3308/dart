import 'dart:math';

class Point{
  num x,y;
  Point(this.x,this.y);

  // 实列化方法
  num distanceTo(Point other){
    var dx = x - other.x;
    var dy = y - other.y;
    return sqrt(dx*dx + dy*dy);
  }

  // 类方法
  static num distanceBetween(Point a, Point b){
    var dx = a.x - b.x;
    var dy = a.y -b.y;
    return sqrt(dx*dx + dy*dy);
  }
}

class Circle{
  final pi = 3.14;
  num r;
  // setter方法
  set l(num l) => r=l/(2*pi);

  // getter 方法
  num get l =>2*pi*r;
  Circle(this.r);
}

void main(){
  var a = Point(2, 2); //9.899494936611665
  var b = Point(9, 9);// 9.899494936611665

  // 调用实列方法
  print(a.distanceTo(b));
  print(Point.distanceBetween(a, b));

  var c = Circle(9);
  print('圆的周长：${c.l}'); //圆的周长：56.52
}