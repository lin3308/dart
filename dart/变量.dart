void main() {
  int i;
  String j;
  Object o;  // 任意类型

  // var 在声明的变量赋值那一刻决定他是声明类型
  var value;
  value = "lance";
  value = 100;
  print(value);

  // 动态运行赋值类型
  dynamic z = "Lance";
  z = 100;

  // 常量  不可修改
  final f = 1;  // 运行时  常量  效率高
  const c = 2;  // 编辑器  常量

  // 区别 
  
}