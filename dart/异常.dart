void main() {

  // 介绍catch的参数
//  try{
//    test();
//  } catch(e,s){
//    // e 就是抛出的异常对象
//    print(e.runtimeType);

//    // s StackTrace 调用栈的信息
//    print(s.runtimeType);

//  }

// 根据不同的异常类型，进行不同的处理
   try{
     test();

   }on Exception catch(e,s){
     print("Exception");

   }on int catch(e){
     print(e);
     print("int");

   }on String catch(e){
     print("String");

   }
}

void test () {
  throw new Exception("111");
  
}