// 与java不同，Dart中没有 interface 关键字，Dart中每个类都隐式的定义一个包含所有实例成员的接口
// 并且这个类实现了这个接口 


class Listener{
  void onComplete(){}
  void onFailure(){}
}

class MyListsner implements Listener{
  MyListsner(){}

  @override
  void onComplete(){}

  @override
  void onFailure(){}


}