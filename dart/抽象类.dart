abstract class Parent{
  String? name;
  // 默认构造方法
  Parent(this.name);

  // 工厂方法返回child实列
  factory Parent.test(String name){
    return new Child(name);
  }
  void printName();

}
// extend 继承抽象类
class Child extends Parent{
  Child(String name):super(name);
  
  @override
  void printName(){
    print(name);
  }
}

void main(){
  var p = Parent.test("Lance");
  print(p.runtimeType); // 输出实际类型 Child
  p.printName(); // Lance
}