// 包括类的定义 如果使用_开头 意味着这个类时private
import 'dart:ffi';

class Point{
  // 使用 _ 开始命名的变量 就是java的private
  int? _x;
  int? y;

  Point(this._x,this.y);

  // 命名构造方法
  Point.Y(this.y);
  Point.X(this._x);

  // 命名构造方法 命名更加直观的表示这个构造方法的作用与特性

  Point.Cool(int y){
    this.y = y;
    print("我好帅");

  }


  // 参数初始化列表 初始化类当中的属性 可以不写方法体
  Point.fromMap(Map map):_x=map["x"],y=map["y"];

  
}

class View{
  View(int context,int attr);
  // 重定向构造方法

  View.a(int context):this(context,0);
}

// 常量构造方法
class ImmutabelPoint{
  final int x;
  final int y;
  // 常量构造方法
  const ImmutabelPoint(this.x,this.y);
}

//  工厂构造方法
// class Manager{
//   int i;
//   static Manager? _instance;
//   factory Manager.getInstance(){
//     if(_instance == null){
//       _instance = new Manager();
//     }
//     return  _instance;
    
//   }
//   Manager();
// }

// class Child extends Manager{

// }



void main(){
  // 使用new来创建 常量构造方法的对象，就和普通的对象没有区别
  // 使用const 来对象，传递参数一样 表示 这几个对象是同一个 表示这个对象是一个编译期常量对象
  var p1 = const ImmutabelPoint(1, 1);
  var p2 = const ImmutabelPoint(1, 1);
  print(p1.hashCode == p2.hashCode);
  print(p1==p2);


}