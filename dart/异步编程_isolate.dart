import 'dart:io';
import 'dart:isolate';

/***
 *  isolate机制
 * Dart是基于单线程模型的语言，但是在开发中我们经常会进行耗时操作比如 网络请求，这种耗时操作会
 * 阻塞我们的代码， Dart也有并发机制，名交isolate 
 * 
 */

int? i;
void main(){
  // 创建一个消息接收器
  var receivePort = new ReceivePort();
  // 将消息接收器当中的发送器 发给子 isolate
  Isolate.spawn(entryPoint, receivePort.sendPort);  // hello

  // 从消息接收器中读取消息

  // 添加消息监听
  // 从消息接收器中读取消息
  receivePort.listen((message) {
    //print(message);
    if(message is SendPort){
      
    }
  });

  // 发送消息
  //receivePort.sendPort.send("1");
 // receivePort.sendPort.send("2");
 // print("1");
}

// void entryPoint(String message){
//   print(message);
//   sleep(Duration(seconds: 10));
// }

void entryPoint(SendPort sendPort){
 // sendPort.send("22222");
  var receivePort = new ReceivePort();
  var sendPort2 = receivePort.sendPort;
  // 发送消息
  sendPort.send(sendPort2);
}