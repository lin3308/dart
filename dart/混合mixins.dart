// Mixins(混入)的类 不能有构造方法  是一种在多类继承中重用一个类代码的方法 它的基本形式如下
class A{
  void a(){
    print("A 的 a 方法");
  }
}

class B{
  void b(){}
  void a(){
    print("B 的a方法");
  }
}
//满足了我们的多继承的需求
class C with A,B{
  void c(){}
  void a(){
   // print("C 的 a方法");
   super.a();

  }
}

// 简写
// C 
//class C = Object with A,B;

void main(){
  // 1. 自身是第一优先级
  // 2.如果自身没有对应的方法 就从with最右边开始查找方法
  var c = new C();
  c.a(); // 
  c.b();
  //c.c();
}

