void main(){
  var list = ['apple','banans','orange'];
  list.forEach((Object element){
    print(element);
  });

  Function perimeter(){
    var pi = 3.14;
    return (num r) => 2*pi*r;
  }

  var per = perimeter();
  var l = per(9);
  print('圆的周长:$l'); //圆的周长:56.52
}