void main(){
  String catName = "字符串";
  print('catName的类型是:${catName.runtimeType}'); // catName的类型是:String
  String strThree = ''' Tody
                        is
                        sunny ''';

  var dogName = '小黄';
  dynamic x = 10;
  print('dogName:${dogName.runtimeType}'); // dogName:String
  print('x:${x.runtimeType}');//x:int

  int? sum;
  String? sub;
  print('sum:${sum}'); // null
  print('sub:${sub}'); // null

  final name = '喵喵村长';

  int i = 1;
  num k = 2;
  print('k的类型:${k.runtimeType}'); // k的类型:int

  double pi = 3.14;
  num w = 1.2;
  print('w的类型:${w.runtimeType}'); // w的类型:double

  int r = 6;
  num L = 37.68;
  print('圆的周长L: ${2*pi*r}'); // 圆的周长L: 37.68
  // 省略{}
  print('周长L: $L'); // 周长L: 37.68

  bool isEmpty = true;
  bool isNull = false;

}