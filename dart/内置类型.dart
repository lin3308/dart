void main() {
  int i = 0; // num 类型

  // 2.字符串
  // 2.1 格式化
  String str = "快扶我去大保健";
  int num = 11;
  String txt = "$str我要找$num号技师";
  //print(txt);

  // 2.2 
  String s = '快扶我去天之道！';
  String s1 = '"test"';
  String s2 = "'test'";

  // boolean

  bool b = false;

  // List 数组
  List<String> list = [];
  List<int> list2 = [1,2,3,4,5];
  // 通过下标获取元素
  int item0 = list2[0];

  // 循环
  
  for(var o in list2){
    //print(o);
  }

  for(var i= 0; i<list2.length; i++){

  }

  // list 对象不可变
  // const 修饰的是[1,2,3] 这一个对象，表示这个对象不可变，不能再add元素
  List<int> list3 = const[1,2,3];
  // list3.add(1);  编译报错

  /***
   *  映射集合 map
   *  key:value
   */
  Map<int,int> map = {1:1,2:2,3:3};

  //print(map[1]);
  //修改value
  map[1] = 100;
  // 添加
  map[4] = 200;
  // 迭代器的变量
  var keys =map.keys;
 // print(keys);
  var values = map.values;

  map.forEach((key, value) {
    print(value);
    print(key);
  });

  /***
   * Runes 特殊字符表示类
   * 
   */

  


  




}