class Car{
  late String length;
  late String color;
}

class Taxi extends Car{
  late double fee;
}

void main(){
  var t = Taxi();
  (t as Car).color = 'Red';
  print('出租车的颜色:${t.color}'); // 出租车的颜色:Red
  print('t的类型:${t.runtimeType}'); // t的类型:Taxi 
  print('t is Car: ${t is Car}'); // t is Car: true
}