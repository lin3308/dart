/**
 * 泛型 
 * 符号<>将List标记为可泛型化的类，即类型可参数化
 * 通常使用一个字母来表示类型参数，例如E,T,S,K,V等
 * 
 * list,set 和map 字面量可以被参数化
 * 
 * class ClassName<T>{
 *  // 泛型参数定义实列变量
 * T t;
 * // 定义方法实列方法 方法返回值和参数都可以使用泛型参数T
 * 
 * T method(List<T> ts){
 *   T ts1;
 *   ts1 = ts[0]
 *   return ts1;
 * }
 * }
 * 
 */

class CachePool<T extends num>{
  final Map pool = Map();
  T getByKey(String key){
    return pool[key];
  }
  void setByKey(String key,T value){
    pool[key] = value;
  }
}

void main(){
  //创建List<String> 类型的实列
  var names = <String>['Seth','Kathy','Lars'];
  // 创建Set<String> 类型的实列
  var uniqueNames = <String>{'seth','kathy','lars'};
  // 创建Map<String,String>类型实列
  var website = <String,String>{
    'qq.com':'腾讯qq',
    'aliyun.com':'阿里云',
    'toutiao.com':'头条',
  };


  var values = [];
  var setValues = Set<String>();
  var mapValues = Map<String,int>();

  // 创建泛型参数我int的实列
  var intMap = CachePool<int>();
  intMap.setByKey('frist', 2); 
  print(intMap.getByKey('frist'));  // 2
  // intMap运行时类型:CachePool<int>
  print('intMap运行时类型:${intMap.runtimeType}');

  // 创建泛型参数为String的实列
  var stringMap = CachePool<double>();
  stringMap.setByKey('hi', 1.1); 
  print(stringMap.getByKey('hi')); // 1.1
  // intMap运行时类型:CachePool<double>
  print('intMap运行时类型:${stringMap.runtimeType}');

}