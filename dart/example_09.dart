class Point{
  // 实列属性
  late num x;
  num y = 0;
  // 类属性 类变量
  static num z =1;
}
void main(){
 Point p = Point();
 var p1 = Point();
 var ponit = Point();

 // 调用属性x的setter方法
 ponit.x = 4;
 // 调用属性x的getter方法
 print('x: ${ponit.x}'); // 4

 // 直接通过类访问类属性
 print('z : ${Point.z}'); // 1
 Point.z = 10;
}