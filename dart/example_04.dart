/* 每一个Dart应用都必须有一个main函数作为入口
main函数的返回为void，它时一个类型list<String>的
可选参数，即可以向其传递数量的参数，即可以向其传递任意数量参数
*/

void main(){
  void printElement(Object element){
    print(element);
  }

  var list = [1,"abc",true];

  /*
  List 集合有一个forEach方法，它的作用是遍历集合中
  的每一个对象，该方法的参数是一个函数，
  它会将当前遍历对象传递给函数
   */
  list.forEach(printElement);
  var show = printElement;
  show('ele');
}