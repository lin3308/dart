/**
 * 声明和使用
 * Dart程序是由被称为库的模块化单元组成的，一个库有个多
 * 顶层声明组成，这些声明可以是函数，变量或类
 * 
 */
// 导入内置库
import 'dart:math';
// 导入第三方包
//import 'package:json_string/json_string.dart';
// 导入本地包
import 'example_01.dart';

void main(){
  // 随机数
  var random = Random();
  var randomInt = random.nextInt(100);
  print('0-100的随机数: $randomInt');
}