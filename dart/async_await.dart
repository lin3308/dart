import 'dart:io';

/***
 *  使用 async 和 await 的代码时异步的，但是看起来很像同步，当我们需要获取A的结果，在执行B时
 *  你需要then() -> then() ,但是利用 async 与await 能够非常好的解决回调地狱的问题
 * 
 */

// async 表示这是一个异步方法，await必须再async方法中使用
// 异步方法只能返回 void和Futrue
Future<String> readFile() async{
  String content = await new File(r"C:\Users\one\Desktop\1.txt").readAsStringSync();
  String contents = await new File(r"C:\Users\one\Desktop\1.txt").readAsStringSync();
  return content;
  
}

void main(){
  // 解决 多写then
  // new File(r"C:\Users\one\Desktop\1.txt").readAsString().then((s){
  //   return new 
  // })
}