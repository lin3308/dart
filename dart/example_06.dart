/*
 语法作用域在变量或对象定义时，就被确定了，
 它的规则以{}为界限，在{}内访问它之外的对象
 在它内部定义的对象不嫩被它的外部访问
*/ 

String topLevel = 'top variable';
void main(){
  var top = topLevel;
  var insideMain = 'insideMain variable';
  void myFunction(){
    var top = topLevel;
    var main = insideMain;
    void instedFunction(){
      var insideFun='insdeFun';

    }
  }

  /**
   * 闭包是一个函数对象，即使函数对象的调用在它原始作用域
   * 之外，依然能够在它语法作用域内的变量
   * 
   */
  //返回一个函数
  Function makeAdder(num addBy){
    return (num i) => addBy +i;
  }
  var add2 = makeAdder(2);
  var result = add2(6);
  print(result); // 8


  /***
   * 作为参数传递的函数就是回调函数，
   * 回调函数还可以接收原函数提供的值
   */
  void printProgress(Function(int) callback){
    for(int progress=0; progress <10;progress++ ){
      callback(progress);
    }
  }

  printProgress((int i){
    print('进度$i');
  });



}