void main() {
  // 方法都是对象，可以赋值给Function变量
  // Function f = fun;
  // f((int i,int j){
  //   return 123;
  // });

 var str2 = fun2((i, j) {
    return "1238";
  });

  print(str2);
  
  
}
// 没有办法表示要传递的这个参数，这个方法需要声明参数，返回什么
void fun(Function f){
  f(1);

}

// 定义一个类型 F类型 这个F类型其实就是一个方法 接受两个int参数 返回void
typedef String F(int i,int j);

String fun2(F f){
  return f(1,2);
}

/**
 * Dart 中可以直接使用回调方法
 */
typedef void onClick();
class Button1{
  void setOnclickListener(onClick listener){
    listener();
  }
}
  
