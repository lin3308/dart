
class Point{
  int x,y;
  Point(this.x,this.y);
}

void printTime(){
  print(DateTime.now());
}

void main(){
  /**
 * 条件运算符
 * 表达式?返回值1：返回值2
 * 表达式为真返回 返回值1 否则 返返回值2
 * 
 * 值1?? 值2 值1不为null 则返回值1 否则返回值2
 * 
 */
  bool isPublic = false;
  // 三元条件表达式
  print('${isPublic ? 'public' :'private'}'); //private

  var x,y = 10,z =20;
  // 二元条件表达式
  print('${x ?? y}'); //10 
  print('${y}'); // 10

  /**
   * 其他运算符
   * () 标识调用函数
   * []: 访问List集合指定位置元素的运算符
   * . 标识访问对象的成员
   * ?. 标识有条件地访问对象的成员，若对象对null，则返回null
   */
  printTime(); // 2022-01-06 22:45:31.778077
  var ls = [1,2,3];
  print(ls[0]); // 1

  var p1 = Point(3,6);
  print('${p1.x}'); // 1

  var p2;
  print('${p2?.x}'); //null

}