/* 枚举类型是一种特殊的类，用于表示固定数量的常量值，、
   enum 关键字来声明一个枚举类，格式如下：
   enum 类名{
     常量值1，
     常量值2，
     常量值3
   }

   可以使用枚举类中常量值的index属性获取该常量在枚举类中的索引
*/

enum Color{
  red,
  green,
  blue
}

void main(){
  print(Color.red); //Color.red
  print(Color.green.index); // 1
  var values = Color.values; 
  print(values); // [Color.red, Color.green, Color.blue]

  switch(Color.blue){
    case Color.red:
       print('红色');
       break;
    case Color.blue:
       print('蓝色'); //蓝色
       break;  
    case Color.green:
       print('绿色');
       break;
    default:
       print('其他颜色');    
  };
}