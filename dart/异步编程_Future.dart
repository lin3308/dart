import 'dart:io';

void main(){
  
  // Future f = Future.delayed(Duration(seconds: 3)); // 延迟
  // // future 的执行 通过then可以获取
  // f.then((value){
  //   print(value);

  // });
  // 读取文件
  new File(r"C:\Users\one\Desktop\1.txt").readAsString().then((String s){
    // 返回void 或者Futrue
    print(s);
  }).catchError((s,e){

  });
}