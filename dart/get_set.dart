class Point{
  // 每一个实例属性，变量都会有一个隐式的 get。非 final 还有set
  int? _x;
  int? _y;

 // x 是方法名
  int? get x => _x;

  set x(int? x)=> _x=x;

  // 运算符重载 定义 + 操作的行为
  //  关键字 operator
  Point operator +(Point other){
    var point = new Point();
    point._x = _x! + other._x!;
    return point;
  }
  bool operator >(Point Other){
    return this._x! > this._y!;
  }


}

void main(){
  // var point = new Point();
  // //get
  // print(point.x);
  // // set
  // point.x = 10;
  // print(point.x);

  var p1 = Point();
  var p2 = Point();

  p1.x = 10;
  p2.x = 20;

  var p3 = p1 + p2;
  print(p3.x); // 30


}