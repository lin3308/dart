void main(){
  // 集合
  var listOne = [1,2,3];
  print('listOne的类型:${listOne.runtimeType}'); // listOne的类型:List<int>

  List<int> listTwo = [3,4,5];
  print('listTwo的类型:${listTwo.runtimeType}'); // listTwo的类型:List<int>

  var listThree = [6,'string',true];
  print('listThree的类型:${listThree.runtimeType}'); // listThree的类型:List<Object>

  // first last length isEmpty
  print('第一个元素:${listThree.first}');
  print('最后一个元素:${listThree.last}');
  print('元素的长度:${listThree.length}');
  print('是否为空:${listThree.isEmpty}');

  listThree.add('up');
  listThree.insert(1, false);
  listThree.remove('string');
  listThree.removeAt(2);


  // 集合set
  var halogens = {"one",'two','three'};
  //halogens的类型:_CompactLinkedHashSet<String>
  print('halogens的类型:${halogens.runtimeType}');

  var dogs= {'abc',1,false};

  print('元素数量:${halogens.length}');
  print('是否为空${halogens.isEmpty}');
  
  dogs.add(false);
  dogs.remove('abc');
  dogs.clear();
  

  // Map集合
  var mapOne = {"first":"partridge","second":'tur',"fifth":'gold'};
  // mapOne的类型:_InternalLinkedHashMap<String, String>
  print('mapOne的类型:${mapOne.runtimeType}');  

  print('mapOne键值对的数量: ${mapOne.length}');// mapOne键值对的数量: 3
  print('mapOne中所有的key:${mapOne.keys}'); // mapOne中所有的key:(first, second, fifth)
  print('mapOne中所有的value:${mapOne.values}'); // mapOne中所有的value:(partridge, tur, gold)

  print(mapOne.containsKey('first')); // true
  print(mapOne.containsValue(1)); // false


}