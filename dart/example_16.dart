import 'dart:math';

 Future <int> getInt() async{
  print('执行getint方法');
  var rng = Random();
  return rng.nextInt(100);
}

void main(){
  // Future 对象 ，它的参数可以是T或Future<T>
  var future = getInt();
  print('futurn运行类型: ${future.runtimeType}');
  future.then((value){
    // 异步执行成功
    print('异步函数返回值:$value'); // 异步函数返回值:85
  }).catchError((onError){
    // 异步函数执行失败
    print('异步函数错误信息:$onError');
  }).whenComplete((){
    // 异步函数执行完成，需要执行的操作
    print('异步函数执行完成'); // 异步函数执行完成
  });
}