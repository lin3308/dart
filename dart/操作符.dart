void main() {
  /***
   * 类型判断操作符
   * 
   */
  num j = 1;
  int i = j as int;

  Object i1 = 1;
  // is  is!
  if(i1 is int){

  }

  // 赋值操作符
  String? k;
  // if(null == k){
  //   k = "123";
  // }
  //k ??= "456";


  /***
   * 条件表达式
   */
  // 三目运算
  // condition?expr1:expr2;

  // var v = k ?? "4564";
  print(k);
  /**
   * 级联操作符 ..
   * 
   */
  new Builder()..a()..b();

  /***
   * 安全操作符 ?.
   * 
   */

  String? str1;
  print(str1?.length);

  
}

class Builder{
  void a(){}
  void b(){}
}