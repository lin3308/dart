void main() {
  fun(10);
  fun2(10);
  fun2(10,30);
  fun3(j:10,i:11);
}

// 可选位置参数
void fun([int? i,int? j]){
  print(i);
  print(j);
}

// 如果想给j传值，就必须保证要给i传值，因为位置 ！！
void fun2([int i =1,int j =2]){
  print(i);
  print(j);
}

// 可选命名参数
void fun3({int? i,int? j}){
  print(i);
  print(j);

}
